heater_power
thermocouple_positive
reed_positive

heater_gnd
thermocouple_negative
ESD
reed_negative


heater_control
thermocouple_input
reed_input

1. Put irons idle and show status in display with set temperatures.
2. If user clicks start button for an iron
  1. Heat up the iron to the set temp.
  2. If user turns knob.
    1. Change temperature and save it to EEPROM
  3. If user press stop button
    1. Stop iron
  4. If user puts iron on stand
    1. wait for X minutes while iron in stand. Afeter time out, do following
      1. Put iron in low temperature mode
      2. Wait for Y minutes while iron in stand. Afeter time out do following
        1. Stop iron
3. Wait for Z minutes while all irons are in idle/stopped state. After time out, do following
  1. Put station in sleep mode
4. If user clicks any button while station in in sleep.
  1. Put station in active mode.
        
      
