#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>
#include <PinChangeInterruptSettings.h>

#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"


/**
 * Configurable data associate with an iron which can be saved in EEPROM
 */
struct IronData {
  int16_t temperature;

};

/**
 * Minimum PWM signal.
 */
const int MINIMUM_POWER = 0;
/**
 * Maximum PWM signal.
 */
const int MAXIMUM_POWER = 255;

/**
 * Minimum temperature allowed, so user cannot go below it.
 */
#define MINIMUM_TEMPERATURE 0.0
/**
 * Maximum temperature allowed, so user cannot go above it.
 */
#define MAXIMUM_TEMPERATURE 400.0
/**
 * Increment/decrement step to use when turning rotary encoder.
 */
#define TEMPERATURE_INCREMENT_STEP 0.5

/**
 * Time interval to update display
 */
#define DISPLAY_REFRESH_TIME 500

class PIDController {
  public:
	/** Proportional **/
	int Kp = 256;

	/** Integral **/
	int Ki = 54;

	/** Derivative **/
	int Kd = 150;

  /**
   * Previous error so it can be used in next calcuation.
   */
  float last_error = 0;

  /**
   * Integration error.
   */
  float i_error = 0;

  /**
   * Time when previous calculations are made.
   */
  unsigned long last_time = 0;

	PIDController() {
    // Ensure PID controller is in reset state.
    this->reset();
	}

  /**
   * Reset PID controller.
   */
  void reset() {
    this->last_time = millis();
    this->i_error = 0;
    this->last_error = 0;
  }

  /**
   * Perform PID calculation and give the result.
   * 
   * @param float current_temperature
   *  Current temperature on iron tip.
   * @param float target_temperature
   *  Temperature to be maintained at iron tip.
   */
  int getChange(float current_temperature, float target_temperature) {
    unsigned long now = millis();
    // Change in time.
    unsigned long dt = this->last_time - now;
    // Proportional
    float error = target_temperature - current_temperature;
    // Integral
    this->i_error += ( error + this->last_error ) * (float) dt;
    // Derivative
    float d_error = (error - this->last_error) / (float) dt;
    // PID output = Proportional gain x current error + Integral gain * past error + Derivative gain x future error
    int output = (int)(((float) this->Kp * error) + ((float) this->Ki * this->i_error) + ((float) this->Kd * d_error));

    // Record current error
    this->last_error = error;
    // Record current time
    this->last_time = now;
    return output;
  }
};


/**
 * Iron is not in use and not powered.
 */
#define IRON_STATE_STOPPED 0
/**
 * Iron is in low temperature mode.
 */
#define IRON_STATE_STANDBY 1
/**
 * User has put iron on holder after use.
 */
#define IRON_STATE_ON_HOLDER 2
/**
 * Iron is being actively used.
 */
#define IRON_STATE_ACTIVE 3

/**
 * Maximum time an iron can be in standby mode.
 * On expire, iron will be powered off.
 */
#define MAXIMUM_STANDBY_TIME 10000 // 5 minutes
/**
 * Maximum time an iron can be in on holder with its set temperature..
 * On expire, iron will be put in stand by mode.
 */
#define MAXIMUM_ON_HOLDER_TIME 10000  // 3 minutes


/**
 * @see https://srdata.nist.gov/its90/download/type_k.tab 
 */
float thermocouple_k_tab1[] = {
    0.0000000E+00,
    2.5173462E+01,
    -1.1662878E+00,
    -1.0833638E+00,
    -8.9773540E-01,
    -3.7342377E-01,
    -8.6632643E-02,
    -1.0450598E-02,
    -5.1920577E-04,
    0.0000000E+00,
};
float thermocouple_k_tab2[] = {
    0.000000E+00,
    2.508355E+01,
    7.860106E-02,
    -2.503131E-01,
    8.315270E-02,
    -1.228034E-02,
    9.804036E-04,
    -4.413030E-05,
    1.057734E-06,
    -1.052755E-08,
};
float thermocouple_k_tab3[] = {
    -1.318058E+02,
    4.830222E+01,
    -1.646031E+00,
    5.464731E-02,
    -9.650715E-04,
    8.802193E-06,
    -3.110810E-08,
    0.000000E+00,
    0.000000E+00,
    0.000000E+00,
};

/**
 * Class representing soldering iron.
 */
class Iron {
  public:
	/**
	 * Pin to which PWM signal for heater element to be applied.
	 */
    uint8_t heater_control_pin;

    /**
     * Analog input pin in which we will read amplified signal from thermocouple.
     */
    uint8_t thermoucouple_reading_pin;

    /**
     * Digital input pin in which reed switch is connected.
     */
    uint8_t reed_input_pin;

    /**
     * Digital input pin where start button for the iron is connected.
     */
    uint8_t start_button_pin;

    /**
  	 * Digital input pin where start button for the iron is connected.
  	 */
    uint8_t stop_button_pin;

    /**
     * Current temperature to be maintained on tip.
     */
    float target_temperature;

    /**
     * Temperature to be maintained on tip whiel iron is in active state.
     */
    float set_temperature;

    /**
     * Iron's current state.
     */
    uint8_t state;

    /**
     * Starting time of active state.
     */
    unsigned long state_start_time;

    /**
     * Current PWM singnal being applied to iron.
     */
    int power;

    /**
     * PID controller for the iron.
     */
    PIDController* pid_controller;

    /**
     * heater_control_pin - PWM pin
     * thermoucouple_reading_pin - Analog input pin
     * reed_input_pin - Digitial pin
     * start_button_pin - Digital pin
     * stop_button_pin - Digital pin
     */
    Iron(uint8_t heater_control_pin, uint8_t thermoucouple_reading_pin, uint8_t reed_input_pin, uint8_t start_button_pin, uint8_t stop_button_pin) {
      this->heater_control_pin = heater_control_pin;
      this->thermoucouple_reading_pin = thermoucouple_reading_pin;
      this->reed_input_pin = reed_input_pin;
      this->start_button_pin = start_button_pin;
      this->stop_button_pin = stop_button_pin;
      this->target_temperature = 0.0;
      this->set_temperature = 0.0;
    }

    /**
     * Initialize iron on device start.
     */
    void init() {
      pinMode(this->heater_control_pin, OUTPUT);
      pinMode(this->reed_input_pin, INPUT_PULLUP);
      pinMode(this->start_button_pin, INPUT_PULLUP);
      pinMode(this->stop_button_pin, INPUT_PULLUP);
      this->pid_controller = new PIDController();

      // Ensure iron is stopped initially.
      this->stop();
    }

    /**
     * Stop iron.
     */
    void stop() {
      this->power = 0;
      // Immediately cut power to iron.
      this->applyPower();

      this->state = IRON_STATE_STOPPED;
      this->state_start_time = millis();
    }

    /**
     * Chnage state of the iron.
     */
    void setState(uint8_t new_state) {
      this->state = new_state;
      this->state_start_time = millis();
    }

    /**
     * Put iron in low temperature mode.
     */
    void putInStandBy() {
      this->target_temperature = 150;
      this->setState(IRON_STATE_STANDBY);
    }

    /**
     * To be called when user puts iron on holder.
     */
    void putOnHolder() {
      this->target_temperature = this->set_temperature;
      this->setState(IRON_STATE_ON_HOLDER);
    }

    /**
     * Activate iron for soldering.
     */
    void activate() {
      this->target_temperature = this->set_temperature;
      this->setState(IRON_STATE_ACTIVE);
    }

    /**
     * Apply PWM signal to the iron.
     */
    void applyPower() {
      analogWrite(this->heater_control_pin, this->power);
    }

    /**
     * Apply correct PWM signal to the iron.
     */
    void maintainTemperature() {
      // Utilize PID controller to calculate the power to be applied.
      this->power += this->pid_controller->getChange(this->readTemperature(), this->target_temperature);
      if (this->power > MAXIMUM_POWER) {
        this->power = MAXIMUM_POWER;
      }
      else if (this->power < MINIMUM_POWER) {
        this->power = MINIMUM_POWER;
      }

      this->applyPower();
    }

    /**
     * Read temperature sensor data and identify current tip  temperature.
     */
    float readTemperature() {
      float voltage = analogRead(this->thermoucouple_reading_pin) * (5.0 / 1023.0);
      // TODO: Measure room (reference) temperature instead of 25 degree.
      return Iron::thermocouple_k_mv_to_temperature(voltage * 1000 / 161.0) + 25.0;
    }

    /**
     * Increment the set temperature.
     */
    void incrementSetTemperature() {
      this->set_temperature += TEMPERATURE_INCREMENT_STEP;
      if (this->set_temperature > MAXIMUM_TEMPERATURE) {
        this->set_temperature = MAXIMUM_TEMPERATURE;
      }
      // Sync set temperature to target temperature
      // if iron is either active or just put on holder.
      if ( this->state == IRON_STATE_ACTIVE || this->state == IRON_STATE_ON_HOLDER ) {
        this->target_temperature = this->set_temperature;
      }
    }

     /**
     * Decrement the set temperature.
     */
    void decrementSetTemperature() {
      this->set_temperature -= TEMPERATURE_INCREMENT_STEP;
      if (this->set_temperature < MINIMUM_TEMPERATURE) {
        this->set_temperature = MINIMUM_TEMPERATURE;
      }
      // Sync set temperature to target temperature
      if ( this->state == IRON_STATE_ACTIVE || this->state == IRON_STATE_ON_HOLDER ) {
        this->target_temperature = this->set_temperature;
      }
    }

    /**
     * Convert thermocouple milli-voltage reading to temperature in celsius.
     * @see https://srdata.nist.gov/its90/download/type_k.tab 
     * 
     * @param mv
     *   Voltage in milli-volts.
     * @return float
     *   Tempearture in celsius.
     */
    static float thermocouple_k_mv_to_temperature(float mv) {
      float *c;
      if (mv >= -5.891 && mv <= 0.0) {
        c = thermocouple_k_tab1;
      } else if (mv > 0.0 && mv <= 20.644) {
          c = thermocouple_k_tab2;
      } else if (mv > 20.644 && mv <= 54.886) {
          c = thermocouple_k_tab3;
      } else {
          return -1.0;
      }
      float t = 0.0;
      for (int p = 0; p < 10; p++) {
          t += *(c + p) * pow(mv, p);
      }
      return t;
    }
};

/**
 * Initial heating up of calibration process.
 * User needs to put the thermocouple in contact with iron tip.
 */
#define CALIBRATOR_STATE_INITIAL_HEATING_UP 1
#define CALIBRATOR_STATE_THERMOCOUPLE_SET_UP 2
#define CALIBRATOR_STATE_COOLING 3
#define CALIBRATOR_STATE_FINAL_HEATING 4

#define CALIBRATOR_HEATING_UP_TEMPERATURE 400.0

/**
 * Class holding calibrator logic.
 */
class IronCalibrator {
  public:
    Iron* iron;

    uint8_t state;

    boolean user_ready;

    IronCalibrator(Iron* i) {
      this->iron = i;
      this->iron->set_temperature = CALIBRATOR_HEATING_UP_TEMPERATURE;
      
      this->state = CALIBRATOR_STATE_INITIAL_HEATING_UP;
      this->user_ready = false;

      this->iron->activate();
    }

    void performCalibration() {
      if (this->state == CALIBRATOR_STATE_INITIAL_HEATING_UP) {
        this->iron->maintainTemperature();
        if (this->iron->readTemperature() >= CALIBRATOR_HEATING_UP_TEMPERATURE && this->user_ready) {
          
        }
      }
    }
};

#define APPLICATION_STATE_USING_IRONS 1
#define APPLICATION_STATE_CALIBRATING_IRON 2

#define DISPLAY_I2C_ADDRESS 0x3C

/**
 * Representation of rotary encoder state.
 */
struct RotaryEncoder {
  boolean changed;
  boolean clockwise;
  float increment;
};


///*** IRON 1 CONNECTED PINS ***/////
/**
 * Arduino pin connected to iron stop button.
 */
#define IRON0_STOP_BUTTON 2

/**
 * Arduino pin connected to iron start button.
 */
#define IRON0_START_BUTTON 3

/**
 * Arduino pin connected to iron reed switch.
 */
#define IRON0_REED_SWITCH_PIN 4

/**
 * Arduino pin connected to iron heating element control.
 */
#define IRON0_HEATER_CONTROL_PIN 5

/**
 * Arduino pin connected to iron thermocouple amplified output.
 */
#define IRON0_THERMOCOUPLE_READING_PIN A6


///*** IRON 2 CONNECTED PINS ***/////
/**
 * Arduino pin connected to iron stop button.
 */
#define IRON1_STOP_BUTTON 6

/**
 * Arduino pin connected to iron start button.
 */
#define IRON1_START_BUTTON 7

/**
 * Arduino pin connected to iron reed switch.
 */
#define IRON1_REED_SWITCH_PIN 8

/**
 * Arduino pin connected to iron heating element control.
 */
#define IRON1_HEATER_CONTROL_PIN 9

/**
 * Arduino pin connected to iron thermocouple amplified output.
 */
#define IRON1_THERMOCOUPLE_READING_PIN A7


/**
 * Class representing the entire device.
 */
class Application {
  public:

    static const int number_of_irons = 1;

    Iron* irons[Application::number_of_irons];

    /**
     * Iron currently being configured/set up.
     */
    uint8_t active_iron;

    /**
     * 128x64 OLED display.
     */
    SSD1306AsciiWire display;

    /**
     * Time when screen is updated last time.
     */
    unsigned long last_display_time;

    /**
     * State of the application/device.
     * Whether iron is being used, calibrated, etc.
     */
    uint8_t state;

    /**
     * Current state of rotary encoder.
     */
    RotaryEncoder rotary_encoder;

    /**
     * Calibrator object.
     * It will be created only as required.
     */
    IronCalibrator* calibrator;


    Application() {
      this->state = APPLICATION_STATE_USING_IRONS;
      this->last_display_time = 0;
      this->calibrator = NULL;
    }

    /**
     * Perform station initialization including irons.
     */
    void init() {
      // Init first iron.
      this->irons[0] = new Iron(IRON0_HEATER_CONTROL_PIN, IRON0_THERMOCOUPLE_READING_PIN, IRON0_REED_SWITCH_PIN, IRON0_START_BUTTON, IRON0_STOP_BUTTON);
      this->irons[0]->init();

      // Make the first iron as active one.
      this->active_iron = 0;

      // Init rotary encoder as idle.
      //this->rotary_encoder.changed = false;

      // Init display.
      Wire.begin();
      display.begin(&Adafruit128x64, DISPLAY_I2C_ADDRESS);
      display.set400kHz();
      display.setFont(Adafruit5x7);
    }

    /**
     * Set the active iron being configured or managed.
     */
    void setActiveIron(uint8_t num) {
      this->active_iron = num % 2;
    }

    /**
     * Show normal front page.
     */
    void displayFrontPage() {
      // Show active iron.
      if (this->number_of_irons == 1) {
        this->display.clear();
        this->display.setCursor(0, 0);
        this->display.set2X();
        this->display.print("1:");
        this->display.set1X();
        this->display.setCursor(16, 0);
        this->display.print("Set: ");
        this->display.println(this->irons[this->active_iron]->set_temperature);
        this->display.setCursor(16, 1);
        this->display.print("Current: ");
        this->display.println(this->irons[this->active_iron]->readTemperature());
        this->display.setCursor(0, 2);
        this->display.print("Status: ");
        if (this->irons[this->active_iron]->state == IRON_STATE_STOPPED) {
          this->display.println("stopped");
        }
        else if (this->irons[this->active_iron]->state == IRON_STATE_STANDBY) {
          this->display.println("stand by");
        }
        else if (this->irons[this->active_iron]->state == IRON_STATE_ON_HOLDER) {
          this->display.println("on holder");
        }
        else {
          this->display.println("active");
        }
        
      }
    }

    /**
     * Display calibration pages.
     */
    void displayCalibrationPage() {
      this->display.clear();
      if (!this->calibrator) {
        this->display.println("Ready for Calibration...");
      }
      else {
        if (this->calibrator->state == CALIBRATOR_STATE_INITIAL_HEATING_UP) {
          this->display.println("Heating up...");
          this->display.setCursor(0, 2);
          this->display.println("Iron tip is heating up");
          this->display.println("Put solder on tip and");
          this->display.println("place thermocouple in");
          this->display.println("contact.");
          this->display.println("Press iron start butt-");
          this->display.println("on when ready.");
        }
      }
    }

    /**
     * Process user inputs if any.
     */
    void processInputs() {
      // User rotated rotary encoder.
//      if (this->rotary_encoder.changed) {
//        if (this->rotary_encoder.clockwise) {
//          this->rotaryEncoderTurningClockwise();
//        }
//        else {
//          this->rotaryEncoderTurningAntiClockwise();
//        }
//        //this->rotaryEncoderTurning();
//        this->rotary_encoder.changed = false;
//      }
    }

    /**
     * Show the correct screen.
     */
    void render() {
      // Refresh screen as necessary.
      if ( millis() - this->last_display_time > DISPLAY_REFRESH_TIME ) {
        if (this->state == APPLICATION_STATE_USING_IRONS) {
          this->displayFrontPage();
        }
        else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
          this->displayCalibrationPage();
        }
        this->last_display_time = millis();
      }
    }

    void processIrons() {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        unsigned long time_now = millis();
        for (int i = 0; i < Application::number_of_irons; i++) {
          switch (this->irons[i]->state) {
  
            case IRON_STATE_STOPPED:
              // TODO: What to do with stopped iron?
              break;
  
            case IRON_STATE_STANDBY:
              if ( ( time_now - this->irons[i]->state_start_time ) >= MAXIMUM_STANDBY_TIME ) {
                // Iron spend enough time in stand by mode.
                // Let's turn it off to save power and for safety.
                this->irons[i]->stop();
              }
              else {
                this->irons[i]->maintainTemperature();
              }
              break;
  
            case IRON_STATE_ON_HOLDER:
              if ( ( time_now - this->irons[i]->state_start_time ) >= MAXIMUM_ON_HOLDER_TIME ) {
                // Iron spend enough time in on holder mode.
                // Let's put it in stand by mode with reduced heating.
                this->irons[i]->putInStandBy();
              }
              else {
                this->irons[i]->maintainTemperature();
              }
              break;
  
            case IRON_STATE_ACTIVE:
            default:
              this->irons[i]->maintainTemperature();
              break;
          }
        }
      }
      else if (this->state == APPLICATION_STATE_CALIBRATING_IRON){
        if (this->calibrator) {
          this->calibrator->performCalibration();
        }
      }
    }

//    /**
//     * Will get called when rotary encoder being tunred clock wise direction.
//     */
//    void rotaryEncoderTurning() {
//      if (this->state == APPLICATION_STATE_USING_IRONS) {
//        this->irons[this->active_iron]->incrementSetTemperature(this.rotary_encoder.increment);
//        this.rotary_encoder.increment = 0;
//        // Sync set temperature to target temperature
//        // if iron is either active or just put on holder.
//        if ( this->irons[this->active_iron]->state == IRON_STATE_ACTIVE || this->irons[this->active_iron]->state == IRON_STATE_ON_HOLDER ) {
//          this->irons[this->active_iron]->target_temperature = this->irons[this->active_iron]->set_temperature;
//        }
//      }
//    }

    /**
     * Will get called when rotary encoder being tunred clock wise direction.
     */
    void rotaryEncoderTurningClockwise() {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        this->irons[this->active_iron]->incrementSetTemperature();
      }
    }

    /**
     * Will get called when rotary encoder being tunred anti-clock wise direction.
     */
    void rotaryEncoderTurningAntiClockwise() {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        this->irons[this->active_iron]->decrementSetTemperature();
      }
    }

    /**
     * Will get called when rotary encoder button is pressed.
     */
    void rotaryEncoderButtonPressed() {
      if ( this->state == APPLICATION_STATE_USING_IRONS ) {
        this->state = APPLICATION_STATE_CALIBRATING_IRON;
        // TODO: Make it to render changes immediately.
      }
      else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
        this->state = APPLICATION_STATE_USING_IRONS;
      }
    }

    void ironStartButtonPressed(uint8_t iron_num) {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
//        if (this->active_iron != iron_num) {
//          this->active_iron = iron_num;
//        }
//        else {
          if (this->irons[this->active_iron]->state == IRON_STATE_STOPPED || this->irons[this->active_iron]->state == IRON_STATE_STANDBY ) {
//            this->irons[this->active_iron]->putOnHolder();
            this->irons[this->active_iron]->state = IRON_STATE_ACTIVE;
          }
//        }
      }
      else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
        if (!this->calibrator) {
          this->calibrator = new IronCalibrator(this->irons[iron_num]);
        }
      }
    }

    void ironStopButtonPressed(uint8_t iron_num) {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        if (this->irons[this->active_iron]->state == IRON_STATE_ACTIVE) {
          this->irons[this->active_iron]->state = IRON_STATE_STANDBY;
        }
        else if (this->irons[this->active_iron]->state == IRON_STATE_STANDBY) {
          this->irons[this->active_iron]->stop();
        }
      }
    }
};

/**
 * Arduino pin connected to rotary encoder pin A.
 */
#define ROTARY_ENCODER_PIN_A A2

/**
 * Arduino pin connected to rotary encoder pin B.
 */
#define ROTARY_ENCODER_PIN_B A3

/**
 * Arduino pin connected to rotary encoder button.
 */
#define ROTARY_ENCODER_BUTTON A1

// One and only application object.
Application app = Application();

/**
 * ISR processing rotary encoder rotations.
 */
void rotary_encoder_changed() {
  if (digitalRead (ROTARY_ENCODER_PIN_A)) {
    app.rotary_encoder.clockwise = digitalRead (ROTARY_ENCODER_PIN_B);
    //app.rotary_encoder.increment += TEMPERATURE_INCREMENT_STEP;
  }
  else {
    app.rotary_encoder.clockwise = !digitalRead (ROTARY_ENCODER_PIN_B);
    //app.rotary_encoder.increment -= TEMPERATURE_INCREMENT_STEP;
  }
  if (app.rotary_encoder.clockwise) {
    app.rotaryEncoderTurningClockwise();
  }
  else {
    app.rotaryEncoderTurningAntiClockwise();
  }
  //app.rotary_encoder.changed = true;
}

/**
 * Interrupt service routine.
 * Rotary encode button has been pressed.
 */
void rotary_encoder_button_pressed() {
  app.rotaryEncoderButtonPressed();
}

/**
 * ISR processing iron0 start button event.
 */
void iron0_start_button_pressed() {
  app.ironStartButtonPressed(0);
}

/**
 * ISR processing iron0 stop button event.
 */
void iron0_stop_button_pressed() {
  app.ironStopButtonPressed(0);
}

/**
 * ISR processing iron0 reed switch state change.
 */
void iron0_reed_switch_state_changed() {
  
}

void setup() {
  // Digital pins for rotary encoder
  pinMode(ROTARY_ENCODER_PIN_A, INPUT_PULLUP);
  pinMode(ROTARY_ENCODER_PIN_B, INPUT_PULLUP);
  pinMode(ROTARY_ENCODER_BUTTON, INPUT_PULLUP);
  attachPCINT(digitalPinToPCINT(ROTARY_ENCODER_PIN_A), rotary_encoder_changed, CHANGE);
  attachPCINT(digitalPinToPCINT(ROTARY_ENCODER_BUTTON), rotary_encoder_button_pressed, FALLING);

  pinMode(IRON0_START_BUTTON, INPUT_PULLUP);
  pinMode(IRON0_STOP_BUTTON, INPUT_PULLUP);
  pinMode(IRON0_REED_SWITCH_PIN, INPUT_PULLUP);
  attachPCINT(digitalPinToPCINT(IRON0_START_BUTTON), iron0_start_button_pressed, FALLING);
  attachPCINT(digitalPinToPCINT(IRON0_STOP_BUTTON), iron0_stop_button_pressed, FALLING);
  attachPCINT(digitalPinToPCINT(IRON0_STOP_BUTTON), iron0_reed_switch_state_changed, CHANGE);

  Serial.begin(9600);
  app.init();
}

void loop() {
  //app.processInputs();
  app.processIrons();
  app.render();
}
