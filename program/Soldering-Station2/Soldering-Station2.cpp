#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>
#include <PinChangeInterruptSettings.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

int16_t EEPROMReadInt16bit(int address) {
  int16_t lsb = EEPROM.read(address);
  int16_t msb = EEPROM.read(address);
  return ( (lsb << 0) & 0xFF) + ( (msb << 8) & 0xFFFF);
}

void EEPROMWriteInt16bit(int address, int16_t value) {
  uint8_t lsb = (value & 0xFF);
  uint8_t msb = ((value >> 8) & 0xFF);
  EEPROM.write(address, lsb);
  EEPROM.write(address + 1, lsb);
}

/**
 * To create objects helping to execute code by a specified interval.
 */
class TimeDivision {
  /**
   * Last time the code is executed by this time division.
   */
  unsigned long last_time;

  public:
  /**
   * Initialize by passing the gap required
   */
  TimeDivision(uint16_t gap) {
    this->last_time = 0;
  }

  /*
   * Tells if this is time to execute.
   */
  bool isTimeToExecute(uint16_t time_gap) {
    return (bool)((millis() - this->last_time) >= time_gap);
  }

  /**
   * Set the gap start time to now.
   */
  void set() {
    this->last_time = millis();
  }
};

/**
 * Address to 4 byte storage.
 * First two bytes are for first iron's temperature
 * and last two bytes for second iron.
 */
#define IRON_TEMPERATURE_STORAGE_ADDRESS 0

/**
 * Configurable data associate with an iron which can be saved in EEPROM
 */
struct IronData {
  int16_t temperature;
};

/**
 * Minimum PWM signal.
 */
const int MINIMUM_POWER = 0;
/**
 * Maximum PWM signal.
 */
const int MAXIMUM_POWER = 255;

/**
 * Minimum temperature allowed, so user cannot go below it.
 */
#define MINIMUM_TEMPERATURE 0.0
/**
 * Maximum temperature allowed, so user cannot go above it.
 */
#define MAXIMUM_TEMPERATURE 550.0
/**
 * Increment/decrement step to use when turning rotary encoder.
 */
#define TEMPERATURE_INCREMENT_STEP 1

/**
 * Time interval to update display
 */
#define DISPLAY_REFRESH_TIME 500

/**
 * Time interval to refresh irons.
 */
#define IRON_REFRESH_TIME 250

class PIDController {
  public:
	/** Proportional **/
	float Kp = 4;

	/** Integral **/
	float Ki = 0.2;

	/** Derivative **/
	float Kd = 1.0;

  /**
   * Previous error so it can be used in next calculation.
   */
  float last_error = 0.0;

  /**
   * Integration error.
   */
  float i_error = 0.0;

  float last_input = 0.0;

  float outputSum = 0.0;

  /**
   * Time when previous calculations are made.
   */
  unsigned long last_time = 0;

	PIDController() {
    // Ensure PID controller is in reset state.
    this->reset();
	}

  /**
   * Reset PID controller.
   */
  void reset() {
    this->last_time = millis();
    this->i_error = 0.0;
    this->last_error = 0.0;

    this->last_input = 0.0;
    this->outputSum = 0.0;
  }

  /**
   * Perform PID calculation and give the result.
   *
   * @param float current_temperature
   *  Current temperature on iron tip.
   * @param float target_temperature
   *  Temperature to be maintained at iron tip.
   */
  int getChange(float current_temperature, float target_temperature) {
    unsigned long now = millis();
    // Change in time.
    unsigned long dt = now - this->last_time;
    // Proportional
    float p_error = target_temperature - current_temperature;
    // Integral
    this->i_error += ( p_error + this->last_error ) * (float) dt;
    // Derivative
    float d_error = (p_error - this->last_error) / (float) dt;
    // PID output = Proportional gain x current error + Integral gain * past error + Derivative gain x future error
    int output = (int)(((float) this->Kp * p_error) + ((float) this->Ki * this->i_error) + ((float) this->Kd * d_error));

    // Record current error
    this->last_error = p_error;
    // Record current time
    this->last_time = now;
    return output;
  }

  /**
   * PID algorithm implementation taken from Arduino
   * PID library: https://github.com/br3ttb/Arduino-PID-Library/blob/9b4ca0e5b6d7bab9c6ac023e249d6af2446d99bb/PID_v1.cpp#L58
   */
  int compute(float current_temperature, float target_temperature) {
    float output = current_temperature;
    unsigned long now = millis();
    unsigned long timeChange = (now - this->last_time);
    // Compute all the working error variables
    double error = target_temperature - current_temperature;
    double dInput = (current_temperature - this->last_input);
    this->outputSum+= (this->Ki * error);

    // Add Proportional on Measurement.
    this->outputSum-= this->Kp * dInput;

    if(this->outputSum > MAXIMUM_POWER) {
      this->outputSum = MAXIMUM_POWER;
    }
    else if(this->outputSum < MINIMUM_POWER) {
      this->outputSum = MINIMUM_POWER;
    }

    double new_output;
    new_output = this->Kp * error;

    /*Compute Rest of PID Output*/
    new_output += this->outputSum - this->Kd * dInput;

    if(new_output > MAXIMUM_POWER) {
      new_output = MAXIMUM_POWER;
    }
    else if(new_output < MINIMUM_POWER) {
      new_output = MINIMUM_POWER;
    }

    /*Remember some variables for next time*/
    this->last_input = current_temperature;
    this->last_time = now;
    return (int) new_output;
  }
};


/**
 * Iron is not in use and not powered.
 */
#define IRON_STATE_STOPPED 0
/**
 * Iron is in low temperature mode.
 */
#define IRON_STATE_STANDBY 1
/**
 * User has put iron on holder after use.
 */
#define IRON_STATE_ON_HOLDER 2
/**
 * Iron is being actively used.
 */
#define IRON_STATE_ACTIVE 3

/**
 * Maximum time in milliseconds an iron can be in standby mode.
 * On expire, iron will be powered off.
 */
#define MAXIMUM_STANDBY_TIME 10000
/**
 * Maximum time in milliseconds an iron can be in on holder with its set temperature..
 * On expire, iron will be put in stand by mode.
 */
#define MAXIMUM_ON_HOLDER_TIME 10000

///*** IRON 1 CONNECTED PINS ***/////
/**
 * Arduino pin connected to iron stop button.
 */
#define IRON0_STOP_BUTTON 2

/**
 * Arduino pin connected to iron start button.
 */
#define IRON0_START_BUTTON 3

/**
 * Arduino pin connected to iron reed switch.
 */
#define IRON0_REED_SWITCH_PIN 4

/**
 * Arduino pin connected to iron heating element control.
 */
#define IRON0_HEATER_CONTROL_PIN 5

/**
 * Arduino pin connected to iron thermocouple amplified output.
 */
#define IRON0_THERMOCOUPLE_READING_PIN A6


///*** IRON 2 CONNECTED PINS ***/////
/**
 * Arduino pin connected to iron stop button.
 */
#define IRON1_STOP_BUTTON 6

/**
 * Arduino pin connected to iron start button.
 */
#define IRON1_START_BUTTON 7

/**
 * Arduino pin connected to iron reed switch.
 */
#define IRON1_REED_SWITCH_PIN 8

/**
 * Arduino pin connected to iron heating element control.
 */
#define IRON1_HEATER_CONTROL_PIN 9

/**
 * Arduino pin connected to iron thermocouple amplified output.
 */
#define IRON1_THERMOCOUPLE_READING_PIN A7

// Pin connected to NTC thermistor.
#define NTC_THERMISTOR_PIN A0


/**
 * Data tables for computing temperature from thermocouple voltage readings.
 * @see https://srdata.nist.gov/its90/download/type_k.tab
 */
float thermocouple_k_tab1[] = {
    0.0000000E+00,
    2.5173462E+01,
    -1.1662878E+00,
    -1.0833638E+00,
    -8.9773540E-01,
    -3.7342377E-01,
    -8.6632643E-02,
    -1.0450598E-02,
    -5.1920577E-04,
    0.0000000E+00,
};
float thermocouple_k_tab2[] = {
    0.000000E+00,
    2.508355E+01,
    7.860106E-02,
    -2.503131E-01,
    8.315270E-02,
    -1.228034E-02,
    9.804036E-04,
    -4.413030E-05,
    1.057734E-06,
    -1.052755E-08,
};
float thermocouple_k_tab3[] = {
    -1.318058E+02,
    4.830222E+01,
    -1.646031E+00,
    5.464731E-02,
    -9.650715E-04,
    8.802193E-06,
    -3.110810E-08,
    0.000000E+00,
    0.000000E+00,
    0.000000E+00,
};

/**
 * Get room temperature.
 * @return float
 */
float readRoomTemperature() {
  // See http://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/
  int adc = analogRead(NTC_THERMISTOR_PIN);
  // TODO: Ensure resistance is 10K for both resistor and thermistor.
  float ntc_resistance = 10000.0 * (1024.0 / (float)adc - 1.0);

  // Calculate temperature with B parameter equation:
  // https://en.wikipedia.org/wiki/Thermistor#B_or_%CE%B2_parameter_equation
  // T = 1 / ( 1/ T0 + 1/B log(R/R0)
  // Need to deduct 273.15 since equation calculation is in Kelvin.
  return 1 / ( (1/298.15) + (1/3950.0) * log(ntc_resistance/ 10000.0)) - 273.15;
}

/**
 * Class representing soldering iron.
 */
class Iron {
  public:
    /**
     * Iron index.
     */
    uint8_t iron_number;

    /**
     * Pin to which PWM signal for heater element to be applied.
     */
    uint8_t heater_control_pin;

    /**
     * Analog input pin in which we will read amplified signal from thermocouple.
     */
    uint8_t thermoucouple_reading_pin;

    /**
     * Digital input pin in which reed switch is connected.
     */
    uint8_t reed_input_pin;

    /**
     * Digital input pin where start button for the iron is connected.
     */
    uint8_t start_button_pin;

    /**
  	 * Digital input pin where start button for the iron is connected.
  	 */
    uint8_t stop_button_pin;

    /**
     * Current temperature to be maintained on tip.
     */
    float target_temperature;

    /**
     * Temperature to be maintained on tip while iron is in active state.
     */
    float set_temperature;

    /**
     * Indicates whether set_temperature value is changes since it is stored in storage.
     */
    bool set_temperature_changed = false;

    /**
     * Timestamp when set_temperature changed.
     */
    unsigned long set_temperature_changed_time = 0;

    /**
     * Iron's current state.
     * Initially all irons will be in stopped state.
     */
    uint8_t state = IRON_STATE_STOPPED;

    /**
     * Starting time of active state.
     */
    unsigned long state_start_time = 0;

    /**
     * Current PWM signal being applied to iron.
     * Initially no power will be applied.
     */
    int power = 0;

    /**
     * Current state of Reed switch.
     */
    uint8_t reed_state = 0;

    /**
     * PID controller for the iron.
     */
    PIDController* pid_controller = NULL;

    /**
     * heater_control_pin - PWM pin
     * thermoucouple_reading_pin - Analog input pin
     * reed_input_pin - Digital pin
     * start_button_pin - Digital pin
     * stop_button_pin - Digital pin
     */
    Iron(uint8_t iron_number, uint8_t heater_control_pin, uint8_t thermoucouple_reading_pin, uint8_t reed_input_pin, uint8_t start_button_pin, uint8_t stop_button_pin) {
      this->iron_number = iron_number;
      this->heater_control_pin = heater_control_pin;
      this->thermoucouple_reading_pin = thermoucouple_reading_pin;
      this->reed_input_pin = reed_input_pin;
      this->start_button_pin = start_button_pin;
      this->stop_button_pin = stop_button_pin;
      this->target_temperature = 0.0;
      // TODO: Read from EEPROM.
      this->set_temperature = 250;
    }

    /**
     * Initialize iron on device start.
     */
    void init() {
      pinMode(this->heater_control_pin, OUTPUT);
      pinMode(this->reed_input_pin, INPUT_PULLUP);
      pinMode(this->start_button_pin, INPUT_PULLUP);
      pinMode(this->stop_button_pin, INPUT_PULLUP);
      this->pid_controller = new PIDController();
      // Get current status of Reed switch.
      this->reed_state = digitalRead(this->reed_input_pin);
      // Ensure iron is stopped initially.
      this->stop();
    }

    /**
     * Get temperature stored in permanence storage.
     */
    void readTemperatureSetting () {
      int address = IRON_TEMPERATURE_STORAGE_ADDRESS + this->iron_number;
      this->set_temperature = EEPROMReadInt16bit(address);
      // Make sure read temperature is within the allowed range.
      if (this->set_temperature > MAXIMUM_TEMPERATURE) {
        this->set_temperature = MAXIMUM_TEMPERATURE;
      }
      else if (this->set_temperature < MINIMUM_TEMPERATURE) {
        this->set_temperature = MINIMUM_TEMPERATURE;
      }
    }

    /**
     * Store temperature in permanence storage.
     */
    void writeTemperatureSetting () {
      int address = IRON_TEMPERATURE_STORAGE_ADDRESS + this->iron_number;
      EEPROMWriteInt16bit(address, this->set_temperature);
    }

    /**
     * Stop iron.
     */
    void stop() {
      this->power = 0;
      // Immediately cut power to iron.
      this->applyPower();

      this->pid_controller->reset();

      this->state = IRON_STATE_STOPPED;
      this->state_start_time = millis();
    }

    /**
     * Change state of the iron.
     */
    void setState(uint8_t new_state) {
      this->state = new_state;
      this->state_start_time = millis();
    }

    /**
     * Put iron in low temperature mode.
     */
    void putInStandBy() {
      this->target_temperature = 150;
      this->setState(IRON_STATE_STANDBY);
    }

    /**
     * To be called when user puts iron on holder.
     */
    void putOnHolder() {
      this->target_temperature = this->set_temperature;
      this->setState(IRON_STATE_ON_HOLDER);
    }

    /**
     * Activate iron for soldering.
     */
    void activate() {
      this->target_temperature = this->set_temperature;
      this->setState(IRON_STATE_ACTIVE);
    }

    /**
     * Apply PWM signal to the iron.
     */
    void applyPower() {
      analogWrite(this->heater_control_pin, this->power);
    }

    /**
     * Apply correct PWM signal to the iron.
     */
    void maintainTemperature() {
      //int change = this->pid_controller->getChange(this->readTemperature(), this->target_temperature);
      // Utilize PID controller to calculate the power to be applied.
      //this->power += change;
      this->power = this->pid_controller->compute(this->readTemperature(), this->target_temperature);
      if (this->power > MAXIMUM_POWER) {
        this->power = MAXIMUM_POWER;
      }
      else if (this->power < MINIMUM_POWER) {
        this->power = MINIMUM_POWER;
      }

      this->applyPower();
    }

    /**
     * Read temperature sensor data and identify current tip  temperature.
     */
    float readTemperature() {
      // Read from ADC pin which connected to amplified signal from
      // thermocouple.
      float voltage = analogRead(this->thermoucouple_reading_pin) * (5.0 / 1023.0);
      // 161 is the gain of our OP07C based signal amplifier, ( (2000 Ohm / 100 Ohm) + 1 ) * (1000 Ohm / 150 Ohm) + 1) = 161
      return Iron::thermocouple_k_mv_to_temperature(voltage * 1000 / 161.0) + readRoomTemperature();
    }

    /**
     * Increment the set temperature.
     */
    void incrementSetTemperature() {
      this->set_temperature += TEMPERATURE_INCREMENT_STEP;
      // Not to set more than allowed.
      if (this->set_temperature > MAXIMUM_TEMPERATURE) {
        this->set_temperature = MAXIMUM_TEMPERATURE;
      }
      // Sync set temperature to target temperature
      // if iron is either active or just put on holder.
      if ( this->state == IRON_STATE_ACTIVE || this->state == IRON_STATE_ON_HOLDER ) {
        this->target_temperature = this->set_temperature;
      }
    }

     /**
     * Decrement the set temperature.
     */
    void decrementSetTemperature() {
      this->set_temperature -= TEMPERATURE_INCREMENT_STEP;
      // Not to set lower than allowed.
      if (this->set_temperature < MINIMUM_TEMPERATURE) {
        this->set_temperature = MINIMUM_TEMPERATURE;
      }
      // Sync set temperature to target temperature if iron is being actively used.
      if ( this->state == IRON_STATE_ACTIVE || this->state == IRON_STATE_ON_HOLDER ) {
        this->target_temperature = this->set_temperature;
      }
    }

    /**
     * It will get called by ISR listening to Reed switch pin.
     */
    void reedStateChanged() {
      // TODO: Check possibility of direct port read to improve performance: https://jeelabs.org/2010/01/06/pin-io-performance/
      this->reed_state = digitalRead(this->reed_input_pin);
      if (this->reed_state && this->state == IRON_STATE_ACTIVE ) {
        // Reed switch is closed and iron is being used.
        this->putOnHolder();
      }
      else if ( (!this->reed_state) && (this->state == IRON_STATE_ON_HOLDER || this->state == IRON_STATE_STANDBY)) {
        // Reed switch is open and iron is either just put on holder or in stand-by mode.
        this->activate();
      }
    }

    /**
     * Whether the iron is on stand or not.
     */
    bool isOnHolder() {
      return this->reed_state;
    }

    /**
     * Convert thermocouple milli-voltage reading to temperature in Celsius.
     * @see https://srdata.nist.gov/its90/download/type_k.tab
     *
     * @param mv
     *   Voltage in milli-volts.
     * @return float
     *   Temperature in Celsius.
     */
    static float thermocouple_k_mv_to_temperature(float mv) {
      float *c;
      if (mv >= -5.891 && mv <= 0.0) {
        c = thermocouple_k_tab1;
      } else if (mv > 0.0 && mv <= 20.644) {
          c = thermocouple_k_tab2;
      } else if (mv > 20.644 && mv <= 54.886) {
          c = thermocouple_k_tab3;
      } else {
          return -1.0;
      }
      float t = 0.0;
      for (int p = 0; p < 10; p++) {
          t += *(c + p) * pow(mv, p);
      }
      return t;
    }
};

/**
 * Initial heating up of calibration process.
 * User needs to put the thermocouple in contact with iron tip.
 */
#define CALIBRATOR_STATE_INITIAL_HEATING_UP 1
#define CALIBRATOR_STATE_THERMOCOUPLE_SET_UP 2
#define CALIBRATOR_STATE_COOLING 3
#define CALIBRATOR_STATE_FINAL_HEATING 4

#define CALIBRATOR_HEATING_UP_TEMPERATURE 400.0

/**
 * Class holding calibrator logic.
 */
class IronCalibrator {
  public:
    Iron* iron;

    uint8_t state;

    bool user_ready;

    IronCalibrator(Iron* i) {
      this->iron = i;
      this->iron->set_temperature = CALIBRATOR_HEATING_UP_TEMPERATURE;

      this->state = CALIBRATOR_STATE_INITIAL_HEATING_UP;
      this->user_ready = false;

      this->iron->activate();
    }

    void performCalibration() {
      if (this->state == CALIBRATOR_STATE_INITIAL_HEATING_UP) {
        this->iron->maintainTemperature();
        if (this->iron->readTemperature() >= CALIBRATOR_HEATING_UP_TEMPERATURE && this->user_ready) {

        }
      }
    }
};

// Various states of application.
// Irons are being used for soldering.
#define APPLICATION_STATE_USING_IRONS 1
// One or both irons are being calibrated.
#define APPLICATION_STATE_CALIBRATING_IRON 2

// I2C address of the display device.
#define DISPLAY_I2C_ADDRESS 0x3C

// Pin connected to buzzer.
#define BUZZER_PIN 10

#define BEEP_SINGLE_DURATION 50

// Beep codes.
#define BEEP_NOTHING 0
#define BEEP_SINGLE 1
#define BEEP_SINGLE_LONG 2

/**
 * Representation of rotary encoder state.
 */
struct RotaryEncoder {
  bool a_set;
  bool b_set;
};

// Buffer to print one line on LCD.
char buffer[20];

TimeDivision
  // Refresh display by 500 milli seconds.
  display_refresh_time = TimeDivision(500),
  // Process irons in each 250 milli seconds.
  iron_process_time = TimeDivision(250),
  // Time gap between beeps.
  beep_time = TimeDivision(250);



/**
 * Class representing the entire device.
 */
class Application {
  public:
    // TODO: Change once ready.
    static const int number_of_irons = 1;

    Iron* irons[Application::number_of_irons];

    /**
     * Iron currently being configured/set up.
     * First iron will be the one always active initially.
     */
    uint8_t active_iron = 0;

    /**
     * 20x4 character LCD display.
     */
    LiquidCrystal_I2C display = LiquidCrystal_I2C(0x27, 20, 4);

    /**
     * Indicates application state change. Mostly used for clearing display.
     */
    bool state_changed = false;

    /**
     * State of the application/device.
     * Whether iron is being used, calibrated, etc.
     */
    uint8_t state;

    /**
     * Current state of rotary encoder.
     */
    RotaryEncoder rotary_encoder;

    /**
     * Calibrator object.
     * It will be created only as required.
     */
    IronCalibrator* calibrator;

    /**
     * To indicate to issue beep(s).
     */
    uint8_t beep = 0;

    uint8_t beep_stage = 0;


    Application() {
      this->state = APPLICATION_STATE_USING_IRONS;
      this->calibrator = NULL;
    }

    /**
     * Perform station initialization including irons.
     */
    void init() {
      // Initialize first iron.
      this->irons[0] = new Iron(0, IRON0_HEATER_CONTROL_PIN, IRON0_THERMOCOUPLE_READING_PIN, IRON0_REED_SWITCH_PIN, IRON0_START_BUTTON, IRON0_STOP_BUTTON);
      this->irons[0]->init();

      // Make the first iron as active one.
      this->active_iron = 0;

      // Initialize LCD.
      this->display.init();
      this->display.backlight();
    }

    /**
     * Set the active iron being configured or managed.
     */
    void setActiveIron(uint8_t num) {
      this->active_iron = num % 2;
    }

    /**
     * Allow to change application state.
     */
    void setState(uint8_t new_state) {
      this->state = new_state;
      // Display needs to be cleared completely since things on
      // screen are going to change entirely.
      this->state_changed = true;
    }

    /**
     * Show normal front page.
     */
    void displayFrontPage() {
      // Show active iron.
      if (this->number_of_irons == 1) {
        // Show both current and set temperatures.
        this->display.setCursor(0, 0);
        sprintf(buffer,"%03d", round(this->irons[this->active_iron]->readTemperature()));
        this->display.print(buffer);
        sprintf(buffer,"/%03d", round(this->irons[this->active_iron]->set_temperature));
        this->display.print(buffer);
        this->display.print((char)223); // Print degree (°) symbol.
        this->display.print("C");

        // Show status of the iron.
        this->display.setCursor(0, 1);
        if (this->irons[this->active_iron]->state == IRON_STATE_STOPPED) {
          this->display.print("Stopped  ");
        }
        else if (this->irons[this->active_iron]->state == IRON_STATE_STANDBY) {
          this->display.print("Stand by ");
        }
        else if (this->irons[this->active_iron]->state == IRON_STATE_ON_HOLDER) {
          this->display.print("On holder");
        }
        else {
          this->display.print("Active   ");
        }

        // Show power level being applied to iron.
        this->display.setCursor(0, 2);
        // TODO: Make icon.
        sprintf(buffer,"Power %03d", this->irons[this->active_iron]->power);
        this->display.print(buffer);

        this->display.setCursor(0, 3);
        // TODO: Make icon to show this.
        this->display.print(this->irons[this->active_iron]->reed_state);

      }
    }

    /**
     * Display calibration pages.
     */
    void displayCalibrationPage() {
      if (!this->calibrator) {
        this->display.setCursor(0, 0);
        this->display.print("Ready for Calibration");
        this->display.setCursor(0, 1);
        this->display.print( (char) 126 );
      }
      else {
        if (this->calibrator->state == CALIBRATOR_STATE_INITIAL_HEATING_UP) {
          this->display.setCursor(0, 0);
          this->display.print("Heating up...");
          this->display.setCursor(0, 1);
          this->display.println("Iron tip is heating up");
//          this->display.println("Put solder on tip and");
//          this->display.println("place thermocouple in");
//          this->display.println("contact.");
//          this->display.println("Press iron start butt-");
//          this->display.println("on when ready.");
        }
      }
    }

    /**
     * Show the correct screen.
     */
    void render() {
      // Refresh screen as necessary.
      if ( display_refresh_time.isTimeToExecute(DISPLAY_REFRESH_TIME) ) {
        if (this->state_changed) {
          this->display.clear();
          this->state_changed = false;
        }
        if (this->state == APPLICATION_STATE_USING_IRONS) {
          this->displayFrontPage();
        }
        else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
          this->displayCalibrationPage();
        }
      }
    }

    /**
     * This function will be called continuously while irons are being used.
     */
    void processIrons() {
      if (iron_process_time.isTimeToExecute(IRON_REFRESH_TIME)) {
        if (this->state == APPLICATION_STATE_USING_IRONS) {
          unsigned long time_now = millis();
          for (int i = 0; i < Application::number_of_irons; i++) {
            switch (this->irons[i]->state) {

              case IRON_STATE_STOPPED:
                // TODO: What to do with stopped iron?
                break;

              case IRON_STATE_STANDBY:
                if ( ( time_now - this->irons[i]->state_start_time ) >= MAXIMUM_STANDBY_TIME ) {
                  // Iron spend enough time in stand by mode.
                  // Let's turn it off to save power and for safety.
                  this->irons[i]->stop();
                }
                else {
                  this->irons[i]->maintainTemperature();
                }
                break;

              case IRON_STATE_ON_HOLDER:
                if ( ( time_now - this->irons[i]->state_start_time ) >= MAXIMUM_ON_HOLDER_TIME ) {
                  // Iron spend enough time in on holder mode.
                  // Let's put it in stand by mode with reduced heating.
                  this->irons[i]->putInStandBy();
                }
                else {
                  this->irons[i]->maintainTemperature();
                }
                break;

              case IRON_STATE_ACTIVE:
              default:
                this->irons[i]->maintainTemperature();
                break;
            }
          }
        }
        else if (this->state == APPLICATION_STATE_CALIBRATING_IRON){
          if (this->calibrator) {
            this->calibrator->performCalibration();
          }
        }
      }
    }

    /**
     * Process requests to make beeps.
     */
    void makeBeep() {
      switch (this->beep) {
      case BEEP_SINGLE:
        // Handle single beep.
        if (this->beep_stage == 0) {
          digitalWrite(BUZZER_PIN, HIGH);
          this->beep_stage++;
          beep_time.set();
        }
        // Stop the beep if met the duration limit.
        else if (this->beep_stage == 1 && beep_time.isTimeToExecute(BEEP_SINGLE_DURATION)) {
          digitalWrite(BUZZER_PIN, LOW);
          this->beep_stage--;
          // Finished handling the beep so reset beep to BEEP_NOTHING
          this->beep = BEEP_NOTHING;
        }
        break;
      case BEEP_NOTHING:
      default:
        break;
      }
    }

    /**
     * It will get called when rotary encoder being turned clock wise direction.
     */
    void rotaryEncoderTurningClockwise() {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        this->irons[this->active_iron]->incrementSetTemperature();
      }
    }

    /**
     * It will get called when rotary encoder being turned anti-clock wise direction.
     */
    void rotaryEncoderTurningAntiClockwise() {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        this->irons[this->active_iron]->decrementSetTemperature();
      }
    }

    /**
     * It will get called by ISR when rotary encoder button is pressed.
     */
    void rotaryEncoderButtonPressed() {
      if ( this->state == APPLICATION_STATE_USING_IRONS ) {
        this->setState(APPLICATION_STATE_CALIBRATING_IRON);
        // TODO: Make it to render changes immediately.
      }
      else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
        this->setState(APPLICATION_STATE_USING_IRONS);
      }
    }

    /**
     * It will get called by ISR when start button is pressed.
     */
    void ironStartButtonPressed(uint8_t iron_num) {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        // Make iron active if it is not already.
        if (this->active_iron != iron_num) {
          this->active_iron = iron_num;
        }
        else if (this->irons[iron_num]->state == IRON_STATE_STOPPED ) {
          if (this->irons[iron_num]->isOnHolder()) {
            // Iron is stopped and on stand.
            // Thus make iron as user just put on stand.
            // So it will become in stand by mode and not heated fully.
            this->irons[iron_num]->putInStandBy();
          }
          else {
            // Iron is not in stand.
            // Thus make iron ready for soldering.
            this->irons[iron_num]->activate();
          }
        }
        else if (this->irons[iron_num]->state == IRON_STATE_STANDBY ) {
          // Iron is in stand by mode and user wants to heat it for soldering.
          // Make as if iron is just put on stand.
          this->irons[iron_num]->putOnHolder();
        }
      }

      else if (this->state == APPLICATION_STATE_CALIBRATING_IRON) {
        if (!this->calibrator) {
          this->calibrator = new IronCalibrator(this->irons[iron_num]);
        }
      }
    }

    /**
     * It will get called by ISR when stop button is pressed.
     */
    void ironStopButtonPressed(uint8_t iron_num) {
      if (this->state == APPLICATION_STATE_USING_IRONS) {
        if (
            this->irons[iron_num]->state == IRON_STATE_ACTIVE ||
            this->irons[iron_num]->state == IRON_STATE_STANDBY
        ) {
          // Always stop iron.
          this->irons[iron_num]->stop();
        }
      }
    }
};

/**
 * Arduino pin connected to rotary encoder pin A.
 */
#define ROTARY_ENCODER_PIN_A A2

/**
 * Arduino pin connected to rotary encoder pin B.
 */
#define ROTARY_ENCODER_PIN_B A3

/**
 * Arduino pin connected to rotary encoder button.
 */
#define ROTARY_ENCODER_BUTTON A1

// One and only application object.
Application app = Application();

///<--- Interrupt Service Routines (ISRs) --->///

/**
 * ISR for rotary encoder pin a
 */
void do_encoder_a() {
  // Low to High transition?
  // TODO: Check possibility of direct port read to improve performance: https://jeelabs.org/2010/01/06/pin-io-performance/
  if (digitalRead(ROTARY_ENCODER_PIN_A) == HIGH) {
    app.rotary_encoder.a_set = true;
    if (!app.rotary_encoder.b_set) {
      // Rotating anti-clockwise.
      app.rotaryEncoderTurningAntiClockwise();
    }
  }

  // High-to-low transition?
  // TODO: Check possibility of direct port read to improve performance: https://jeelabs.org/2010/01/06/pin-io-performance/
  if (digitalRead(ROTARY_ENCODER_PIN_A) == LOW) {
    app.rotary_encoder.a_set = false;
  }
}

/**
 * ISR for rotary encoder pin a
 */
void do_encoder_b() {
  // Low-to-high transition?
  // TODO: Check possibility of direct port read to improve performance: https://jeelabs.org/2010/01/06/pin-io-performance/
  if (digitalRead(ROTARY_ENCODER_PIN_B) == HIGH) {
    app.rotary_encoder.b_set = true;
    if (!app.rotary_encoder.a_set) {
      // Rotating Clockwise.
      app.rotaryEncoderTurningClockwise();
    }
  }

  // High-to-low transition?
  // TODO: Check possibility of direct port read to improve performance: https://jeelabs.org/2010/01/06/pin-io-performance/
  if (digitalRead(ROTARY_ENCODER_PIN_B) == LOW) {
    app.rotary_encoder.b_set = false;
  }
}

/**
 * Interrupt service routine.
 * Rotary encode button has been pressed.
 */
void rotary_encoder_button_pressed() {
  app.rotaryEncoderButtonPressed();
}

/**
 * ISR processing iron0 start button event.
 */
void iron0_start_button_pressed() {
  app.ironStartButtonPressed(0);
}

/**
 * ISR processing iron0 stop button event.
 */
void iron0_stop_button_pressed() {
  app.ironStopButtonPressed(0);
}

/**
 * ISR processing iron0 reed switch state change.
 */
void iron0_reed_switch_state_changed() {
  app.irons[0]->reedStateChanged();
}

/**
 * Set up device for operation.
 */
void setup() {
  // Digital pins for rotary encoder
  pinMode(ROTARY_ENCODER_PIN_A, INPUT);
  pinMode(ROTARY_ENCODER_PIN_B, INPUT);
  pinMode(ROTARY_ENCODER_BUTTON, INPUT);
  attachPCINT(digitalPinToPCINT(ROTARY_ENCODER_PIN_A), do_encoder_a, CHANGE);
  attachPCINT(digitalPinToPCINT(ROTARY_ENCODER_PIN_B), do_encoder_b, CHANGE);
  attachPCINT(digitalPinToPCINT(ROTARY_ENCODER_BUTTON), rotary_encoder_button_pressed, RISING);

  pinMode(IRON0_START_BUTTON, INPUT_PULLUP);
  pinMode(IRON0_STOP_BUTTON, INPUT_PULLUP);
  pinMode(IRON0_REED_SWITCH_PIN, INPUT_PULLUP);
  attachPCINT(digitalPinToPCINT(IRON0_START_BUTTON), iron0_start_button_pressed, RISING);
  attachPCINT(digitalPinToPCINT(IRON0_STOP_BUTTON), iron0_stop_button_pressed, RISING);
  attachPCINT(digitalPinToPCINT(IRON0_REED_SWITCH_PIN), iron0_reed_switch_state_changed, CHANGE);

  Serial.begin(9600);
  app.init();
}

void loop() {
  app.processIrons();
  app.render();
  app.makeBeep();
}
