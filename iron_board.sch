EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:diy
LIBS:soldering-station-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Soldering Station - Iron Board"
Date "2018-07-07"
Rev "1"
Comp "Detlus"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Screw_terminal_3P IP1
U 1 1 5B41570F
P 5870 1320
F 0 "IP1" V 5970 1265 50  0000 L BNN
F 1 "24VDC" V 5890 1215 50  0000 L BNN
F 2 "diy:Screw_terminal_3P" H 5870 1320 50  0001 L BNN
F 3 "691212710003" H 5870 1320 50  0001 L BNN
F 4 "T/BLOCK, 5MM, PCB, 3WAY; Connector Type: Terminal Block, PCB; Series: 212; Connector Mounting: PCB; Pitch Spacing: 5m..." H 5870 1320 50  0001 L BNN "Description"
F 5 "Würth Elektronik" H 5870 1320 50  0001 L BNN "MF"
	1    5870 1320
	0    1    -1   0   
$EndComp
$Comp
L Q_NMOS_GDS Q4
U 1 1 5B415716
P 5310 3120
F 0 "Q4" H 5610 3170 50  0000 R CNN
F 1 "IRLZ44N" H 5810 3070 50  0000 R CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 5510 3220 50  0001 C CNN
F 3 "" H 5310 3120 50  0000 C CNN
	1    5310 3120
	1    0    0    -1  
$EndComp
$Comp
L R DR1
U 1 1 5B415723
P 3030 3495
F 0 "DR1" V 3110 3495 50  0000 C CNN
F 1 "10K" V 3030 3495 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2960 3495 50  0001 C CNN
F 3 "" H 3030 3495 50  0000 C CNN
	1    3030 3495
	0    1    1    0   
$EndComp
$Comp
L R DR2
U 1 1 5B41572A
P 3185 3715
F 0 "DR2" V 3265 3715 50  0000 C CNN
F 1 "10K" V 3185 3715 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3115 3715 50  0001 C CNN
F 3 "" H 3185 3715 50  0000 C CNN
	1    3185 3715
	0    1    1    0   
$EndComp
$Comp
L R DR3
U 1 1 5B415731
P 3625 2710
F 0 "DR3" V 3705 2710 50  0000 C CNN
F 1 "1K" V 3625 2710 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3555 2710 50  0001 C CNN
F 3 "" H 3625 2710 50  0000 C CNN
	1    3625 2710
	-1   0    0    1   
$EndComp
$Comp
L Q_NPN_BEC Q1
U 1 1 5B415738
P 3525 3495
F 0 "Q1" H 3770 3575 50  0000 R CNN
F 1 "MMBT4401" H 3550 3655 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3725 3595 50  0001 C CNN
F 3 "" H 3525 3495 50  0000 C CNN
	1    3525 3495
	1    0    0    -1  
$EndComp
$Comp
L Q_PNP_BEC Q3
U 1 1 5B41573F
P 4435 2730
F 0 "Q3" H 4735 2780 50  0000 R CNN
F 1 "MMBT4403" H 5035 2680 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4635 2830 50  0001 C CNN
F 3 "" H 4435 2730 50  0000 C CNN
	1    4435 2730
	1    0    0    1   
$EndComp
$Comp
L Q_NPN_BEC Q2
U 1 1 5B415746
P 4435 3495
F 0 "Q2" H 4725 3465 50  0000 R CNN
F 1 "MMBT4401" H 4980 3390 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4635 3595 50  0001 C CNN
F 3 "" H 4435 3495 50  0000 C CNN
	1    4435 3495
	1    0    0    -1  
$EndComp
$Comp
L R DR5
U 1 1 5B41574D
P 4040 2730
F 0 "DR5" V 4120 2730 50  0000 C CNN
F 1 "1K" V 4040 2730 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3970 2730 50  0001 C CNN
F 3 "" H 4040 2730 50  0000 C CNN
	1    4040 2730
	0    1    1    0   
$EndComp
$Comp
L R DR4
U 1 1 5B415754
P 4040 3495
F 0 "DR4" V 4120 3495 50  0000 C CNN
F 1 "1K" V 4040 3495 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3970 3495 50  0001 C CNN
F 3 "" H 4040 3495 50  0000 C CNN
	1    4040 3495
	0    1    1    0   
$EndComp
$Comp
L R DR6
U 1 1 5B41575B
P 4810 3120
F 0 "DR6" V 4890 3120 50  0000 C CNN
F 1 "10R" V 4810 3120 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4740 3120 50  0001 C CNN
F 3 "" H 4810 3120 50  0000 C CNN
	1    4810 3120
	0    1    1    0   
$EndComp
$Comp
L R DR7
U 1 1 5B415762
P 5060 3420
F 0 "DR7" V 5140 3420 50  0000 C CNN
F 1 "4K7" V 5060 3420 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4990 3420 50  0001 C CNN
F 3 "" H 5060 3420 50  0000 C CNN
	1    5060 3420
	-1   0    0    1   
$EndComp
Text Label 5870 1745 0    60   ~ 0
24VDC
Text Label 2815 3495 2    60   ~ 0
HEATER_CONTROL
Text Label 2840 1865 0    60   ~ 0
HEATER_CONTROL
Text Label 2840 1965 0    60   ~ 0
THERMOCOUPLE_READING
Text Label 2840 2065 0    60   ~ 0
REED_INPUT
Text Label 5910 4695 0    60   ~ 0
THERMOCOUPLE_READING
Text Label 7110 2545 2    60   ~ 0
TC_P
Text Label 7110 2645 2    60   ~ 0
TC_N
Text Label 7110 2745 2    60   ~ 0
REED_INPUT
Text Label 7110 2845 2    60   ~ 0
REED_GND
Text Label 5410 3715 0    60   ~ 0
IRON_GND
Text Label 6465 2845 2    60   ~ 0
IRON_GND
Text Label 5785 1940 0    60   ~ 0
HEATER_GND
Text Label 5970 1575 0    60   ~ 0
IRON_GND
Text Label 2845 2630 0    60   ~ 0
IRON_GND
$Comp
L OP07C O1
U 1 1 5B415786
P 3320 4695
F 0 "O1" H 2905 4700 60  0000 C CNN
F 1 "OP07C" H 3435 4460 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 3320 4695 60  0001 C CNN
F 3 "" H 3320 4695 60  0000 C CNN
	1    3320 4695
	1    0    0    -1  
$EndComp
$Comp
L R OR1
U 1 1 5B41578D
P 2560 4870
F 0 "OR1" V 2640 4870 50  0000 C CNN
F 1 "100" V 2560 4870 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2490 4870 50  0001 C CNN
F 3 "" H 2560 4870 50  0000 C CNN
	1    2560 4870
	0    1    1    0   
$EndComp
Text Label 2135 4470 0    60   ~ 0
TC_P
Text Label 2135 4870 0    60   ~ 0
TC_N
Text Label 2865 5215 0    60   ~ 0
IRON_GND
$Comp
L R OR2
U 1 1 5B415797
P 3525 5080
F 0 "OR2" V 3605 5080 50  0000 C CNN
F 1 "2K" V 3525 5080 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3455 5080 50  0001 C CNN
F 3 "" H 3525 5080 50  0000 C CNN
	1    3525 5080
	0    1    1    0   
$EndComp
$Comp
L CONN_01X05 MC1
U 1 1 5B41579E
P 2640 2065
F 0 "MC1" H 2640 2365 50  0000 C CNN
F 1 "TO_MCU" V 2740 2065 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 2640 2065 50  0001 C CNN
F 3 "" H 2640 2065 50  0000 C CNN
	1    2640 2065
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5B4157A5
P 2645 2580
F 0 "P1" H 2645 2730 50  0000 C CNN
F 1 "POWER" V 2745 2580 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2645 2580 50  0001 C CNN
F 3 "" H 2645 2580 50  0000 C CNN
	1    2645 2580
	-1   0    0    1   
$EndComp
Text Label 2895 2530 0    60   ~ 0
IB_VCC
Text Label 3545 4140 0    60   ~ 0
IB_VCC
$Comp
L OP07C O2
U 1 1 5B4157AE
P 5255 4695
F 0 "O2" H 4805 4695 60  0000 C CNN
F 1 "OP07C" H 5415 4480 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5255 4720 60  0001 C CNN
F 3 "" H 5255 4720 60  0000 C CNN
	1    5255 4695
	1    0    0    -1  
$EndComp
$Comp
L R OR3
U 1 1 5B4157B5
P 4510 4870
F 0 "OR3" V 4590 4870 50  0000 C CNN
F 1 "150" V 4510 4870 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4440 4870 50  0001 C CNN
F 3 "" H 4510 4870 50  0000 C CNN
	1    4510 4870
	0    1    1    0   
$EndComp
$Comp
L R OR4
U 1 1 5B4157BC
P 5465 5080
F 0 "OR4" V 5545 5080 50  0000 C CNN
F 1 "1K" V 5465 5080 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5395 5080 50  0001 C CNN
F 3 "" H 5465 5080 50  0000 C CNN
	1    5465 5080
	0    1    1    0   
$EndComp
Text Label 2840 2265 0    60   ~ 0
STOP_BUTTON
Text Label 2840 2165 0    60   ~ 0
START_BUTTON
$Comp
L CONN_01X02 B1
U 1 1 5B4157C5
P 7335 3350
F 0 "B1" H 7335 3500 50  0000 C CNN
F 1 "START_BUTTON" V 7435 3350 50  0000 C CNN
F 2 "diy:JST_XH_2P-2.54" H 7335 3350 50  0001 C CNN
F 3 "" H 7335 3350 50  0000 C CNN
	1    7335 3350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 B2
U 1 1 5B4157CC
P 7335 3965
F 0 "B2" H 7335 4115 50  0000 C CNN
F 1 "STOP_BUTTON" V 7435 3965 50  0000 C CNN
F 2 "diy:JST_XH_2P-2.54" H 7335 3965 50  0001 C CNN
F 3 "" H 7335 3965 50  0000 C CNN
	1    7335 3965
	1    0    0    -1  
$EndComp
Text Label 7135 3300 2    60   ~ 0
IRON_GND
Text Label 7135 3400 2    60   ~ 0
START_BUTTON
Text Label 7135 3915 2    60   ~ 0
IRON_GND
Text Label 7135 4015 2    60   ~ 0
STOP_BUTTON
Wire Wire Line
	4960 3120 5110 3120
Wire Wire Line
	5060 3270 5060 3120
Connection ~ 5060 3120
Wire Wire Line
	4535 2930 4535 3295
Wire Wire Line
	4660 3120 4535 3120
Connection ~ 4535 3120
Wire Wire Line
	4190 2730 4235 2730
Wire Wire Line
	3625 2860 3625 3295
Wire Wire Line
	4190 3495 4235 3495
Wire Wire Line
	3890 2730 3890 3495
Wire Wire Line
	3890 3080 3625 3080
Connection ~ 3625 3080
Connection ~ 3890 3080
Wire Wire Line
	5870 1520 5870 1840
Wire Wire Line
	2845 2530 4535 2530
Wire Wire Line
	3180 3495 3325 3495
Wire Wire Line
	5410 1940 5410 2920
Wire Wire Line
	5410 3715 3335 3715
Wire Wire Line
	3625 3715 3625 3695
Wire Wire Line
	4535 3715 4535 3695
Connection ~ 3625 3715
Connection ~ 4535 3715
Wire Wire Line
	5060 3570 5410 3570
Wire Wire Line
	3035 3715 2815 3715
Wire Wire Line
	2815 3715 2815 3495
Wire Wire Line
	5870 1840 7090 1840
Wire Wire Line
	2740 5080 3375 5080
Wire Wire Line
	2710 4870 2770 4870
Wire Wire Line
	2740 5080 2740 4870
Connection ~ 2740 4870
Wire Wire Line
	2770 4470 2135 4470
Wire Wire Line
	2135 4870 2410 4870
Wire Wire Line
	2385 4870 2385 5215
Connection ~ 2385 4870
Wire Wire Line
	2385 5215 5080 5215
Wire Wire Line
	4050 5080 3675 5080
Wire Wire Line
	4050 4470 4050 5080
Wire Wire Line
	4050 4695 3945 4695
Wire Wire Line
	3545 4140 3545 4320
Wire Wire Line
	3625 2530 3625 2560
Wire Wire Line
	4705 4470 4050 4470
Connection ~ 4050 4695
Wire Wire Line
	4360 4870 4165 4870
Wire Wire Line
	4165 4870 4165 5215
Wire Wire Line
	4660 4870 4705 4870
Wire Wire Line
	4680 4870 4680 5080
Wire Wire Line
	4680 5080 5315 5080
Connection ~ 4680 4870
Wire Wire Line
	5080 5215 5080 5095
Connection ~ 4165 5215
Wire Wire Line
	5615 5080 5910 5080
Wire Wire Line
	5910 5080 5910 4695
Wire Wire Line
	3545 4140 5480 4140
Wire Wire Line
	5480 4140 5480 4320
Wire Wire Line
	5970 1520 5970 1575
Wire Wire Line
	3145 5095 3145 5215
Connection ~ 3145 5215
Wire Wire Line
	5410 3320 5410 3715
Connection ~ 5410 3570
Connection ~ 3625 2530
NoConn ~ 3095 4220
NoConn ~ 3320 4270
NoConn ~ 5030 4220
NoConn ~ 5255 4270
Wire Wire Line
	5910 4695 5880 4695
Wire Wire Line
	2815 3495 2880 3495
Text Label 5855 2040 0    60   ~ 0
ESD
$Comp
L JST_XH_3P-2.54 TO_IRON1
U 1 1 5B61E3D9
P 7290 1940
F 0 "TO_IRON1" H 7300 2160 60  0000 C CNN
F 1 "JST_XH_3P-2.54" V 7465 2040 60  0000 C CNN
F 2 "diy:JST_XH_3P-2.54" H 7290 1940 60  0001 C CNN
F 3 "" H 7290 1940 60  0000 C CNN
	1    7290 1940
	1    0    0    -1  
$EndComp
Wire Wire Line
	7090 1940 5410 1940
Wire Wire Line
	7090 2040 5770 2040
$Comp
L JST_XH_5P-2.54 TO_IRON2
U 1 1 5B61EF75
P 7310 2745
F 0 "TO_IRON2" H 7310 3070 60  0000 C CNN
F 1 "JST_XH_5P-2.54" V 7485 2845 60  0000 C CNN
F 2 "diy:JST_XH_5P-2.54" H 7310 2745 60  0001 C CNN
F 3 "" H 7310 2745 60  0000 C CNN
	1    7310 2745
	1    0    0    -1  
$EndComp
NoConn ~ 7110 2945
Wire Wire Line
	7110 2845 6465 2845
Wire Wire Line
	5770 2040 5770 1520
$EndSCHEMATC
